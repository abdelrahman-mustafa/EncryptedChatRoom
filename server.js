const express = require('express')
const app = express()
const rsawrapper = require('./components/rsa-wrapper');

rsawrapper.initLoadKeys(__dirname);


//set the template engine ejs
app.set('view engine', 'ejs')

//middlewares
app.use(express.static('public'))


//routes
app.get('/', (req, res) => {
	res.render('index')
})

//Listen on port 3000
server = app.listen(3000)



//socket.io instantiation
const io = require("socket.io")(server)


//listen on every connection
io.on('connection', (socket) => {
	console.log('New user connected')

	//default username
	socket.username = "Anonymous"

    //listen on change_username
    socket.on('change_username', (data) => {
        socket.username = data.username
    })

    //listen on new_message
    socket.on('new_message', (data) => {
        // decrpt the data comming from client (encrypted by server public key)
        let decryptedData = rsawrapper.decrypt(rsawrapper.serPri, data.message);
        // encrypt the message with cliPub
        let encryptedData = rsawrapper.encrypt(rsawrapper.cliPub,decryptedData);
        //broadcast the new message 
        io.sockets.emit('new_message', {message : encryptedData, username : socket.username});
    })

    //listen on typing
    socket.on('typing', (data) => {
    	socket.broadcast.emit('typing', {username : socket.username})
    })
})
