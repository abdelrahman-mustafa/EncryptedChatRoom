
 var socket = io.connect('http://localhost:3000')

 //buttons and inputs
 var message = $("#message")
 var username = $("#username")
 var send_message = $("#send_message")
 var send_username = $("#send_username")
 var chatroom = $("#chatroom")
 var feedback = $("#feedback")
 
 //Emit message
 send_message.click(function(){
     // encrypt message with public key of server 
     rsawrapper.publicEncrypt(document.getElementById('server_public').value, message.val()).then(function (encrypted){
        socket.emit('new_message', {message : encrypted})
     });
 });
 
 //Listen on new_message
 socket.on("new_message", (data) => {
     feedback.html('');
     message.val('');
     // decrypt message 
     rsawrapper.privateDecrypt(document.getElementById('client_private').value, data.message).then(function (decryptedMessage){
        chatroom.append("<p class='message'>" + data.username + ": " + decryptedMessage + "</p>")
 
     });
 });
 
 //Emit a username
 send_username.click(function(){
     socket.emit('change_username', {username : username.val()})
 });
 
 //Emit typing
 message.bind("keypress", () => {
     socket.emit('typing')
 });
 
 //Listen on typing
 socket.on('typing', (data) => {
     feedback.html("<p><i>" + data.username + " is typing a message..." + "</i></p>")
 });
 