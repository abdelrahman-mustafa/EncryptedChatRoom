(function(){
'use strict';
const crypto = window.crypto.subtle;
const rsaParams = {name: "RSA-OAEP", hash:{name: "SHA-1"}};

function importPublicKey(keyInPemFormat){
return new Promise(function (resolve, reject){
    var key = converwrapper.convertPemToBinary2(keyInPemFormat);
    key = converwrapper.base64StringToArrayBuffer(key);
    crypto.importKey('spki', key, rsaParams, false, ['encrypt']) 
    .then(function(cryptoKey){
        resolve(cryptoKey);
    });
});
}

function importPrivateKey(keyInPemFormat){
    var key = converwrapper.convertPemToBinary2(keyInPemFormat);
    key = converwrapper.base64StringToArrayBuffer(key);
    return new Promise(function(resolve, reject){
    crypto.importKey('pkcs8', key, rsaParams, false, ["decrypt"])
    .then(function(cryptokey) {
    resolve(cryptokey);
    });
    });
}

function publicEncrypt(keyInPemFormat, message){
    return new Promise(function(resolve, reject){
        importPublicKey(keyInPemFormat).then(function (key){
            crypto.encrypt(rsaParams, key, converwrapper.str2abUtf8(message))
            .then(function (encrypted){
                resolve(converwrapper.arrayBufferToBase64String(encrypted));
            })
        });
    });
}
// decrypt with private key. Private key in pkcs8 format, message must be base64 encoded:
function privateDecrypt(keyInPemFormat, encryptedBase64Message) {
return new Promise(function(resolve, reject){
importPrivateKey(keyInPemFormat).then(function (key) {
crypto.decrypt(rsaParams, key, converwrapper.base64StringToArrayBuffer(encryptedBase64Message))
.then(function(decrypted){
resolve(converwrapper.arrayBufferToUtf8(decrypted));
});
});
});
}

window.rsawrapper = {
importPrivateKey: importPrivateKey,
importPublicKey: importPublicKey,
privateDecrypt: privateDecrypt,
publicEncrypt: publicEncrypt
}

}());