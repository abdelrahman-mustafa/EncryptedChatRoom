const path = require('path');
const rsawrapper = {};
const fs = require('fs');
const nodeRsa = require('node-rsa');
const crypto = require('crypto');

// generate keys (public - private) for client and server 
rsawrapper.generate = (direction)=>{
let key = new nodeRsa();
// generate key with argu (65537) and size (2048)
key.generateKeyPair(2048, 65537);
// save keys as pem line pkcs8 (formated)
fs.writeFileSync(path.resolve(__dirname, 'keys', direction+ '.private.pem'), key.exportKey('pkcs8-private-pem'));
fs.writeFileSync(path.resolve(__dirname, 'keys', direction+ '.public.pem'), key.exportKey('pkcs8-public-pem'));
return true;
};
// encrypt message 
rsawrapper.encrypt = (publicKey, message)=>{
    let enc = crypto.publicEncrypt({
        key: publicKey,
        padding: crypto.RSA_PKCS1_OAEP_PADDING
    },Buffer.from(message));
    return enc.toString('base64');
};
// decrypt message 
rsawrapper.decrypt = (privateKey, message)=>{
    let enc = crypto.privateDecrypt({
        key: privateKey,
        padding: crypto.RSA_PKCS1_OAEP_PADDING
    },Buffer.from(message, 'base64'));
    return enc.toString();
};
//,,
rsawrapper.initLoadKeys = (basepath)=>{
    rsawrapper.serPub = fs.readFileSync(path.resolve(basepath,'keys','server.public.pem'));
    rsawrapper.serPri = fs.readFileSync(path.resolve(basepath,'keys','server.private.pem'));
    rsawrapper.cliPub = fs.readFileSync(path.resolve(basepath,'keys','client.public.pem'));
};


module.exports = rsawrapper;